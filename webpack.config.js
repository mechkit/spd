var path = require('path');
var webpack = require('webpack');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
//var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  target: 'web',
  entry: {
    app: ['./src/app.js'],
  },
  output: {
    //path: path.resolve(__dirname, 'public'),
    path: path.resolve(__dirname, 'public'),
    publicPath: '/',
    filename: '[name].js'
  },
  //plugins: [
  //  new webpack.DefinePlugin({
  //    'process.env.NODE_ENV': JSON.stringify('dev')
  //  })
  //],
  devtool: 'eval-source-map',

  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress: true,
    port: 9000
  },
  watchOptions: {
    ignored: [
      /node_modules([\\]+|\/)+(?!markdown_loader)/,
      /markdown_loader([\\]+|\/)node_modules/
    ]
  },
  plugins: [
    new webpack.IgnorePlugin(/jsdom/),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    // new BundleAnalyzerPlugin(),
    // new webpack.DefinePlugin({
    //   'process.env.NODE_ENV': '"production"'
    // }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        include: /src/,
        //exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [{ loader: 'style-loader' }, { loader: 'css-loader' }],
      },
      {
        test: require.resolve('spd_algorithm'),
        use: 'imports-loader?jsdom=>false'
      }
    ]

  }

};
