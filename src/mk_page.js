import {div, span, p, a, ul, li, br, h1, h2, h3, input, select, option} from 'specdom_helper';
import titlebar_spec from './titlebar_spec';
import page_spd from './page_spd';
import $ from 'specdom';




export default function(actions, selected_page_id){
  var pages_content = {
    'default': page_spd(actions),
    'about': {
      tag: 'div',
      props: {
        class: 'level_0'
      },
      meta: {},
      children: ['About'],
    },
    '404': div({class:'level_0'}, [
      'Unknown link. ',
      br(),
      'Return ',
      a('home','/')
    ])
  };

  var page = pages_content[selected_page_id];
  var page_content_specs;
  if( ! page ){
    page_content_specs = pages_content[404];
  } else {
    page_content_specs = page;
  }

  // var titlebar_content = titlebar_spec('Solar Plans Designer', ['about'], selected_page_id);
  var titlebar_content = titlebar_spec('Solar Plans Designer', [], selected_page_id);

  var specs = {
    tag: 'div',
    children: [
      {
        tag: 'div',
        props: {
          id: 'titlebar'
        },
        children: [titlebar_content]
      },
      div({class:'transition'}),
      div({id:'background'},[
        div({class:'display_area'},[
          page_content_specs
        ])
      ])
    ]
  };

  var content_specdom = $('#content');
  //rint_specs( specsspan, console.log, 's| ');
  var status = content_specdom.append(specs);
  if( status ){
    console.log('Load status:', status);
    actions.init();
  }
}
