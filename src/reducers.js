import mk_system_data from './mk_system_data';
import f from 'functions';

export default {

  init: function(state,action){
    console.log('reducer: init');
    return state;
  },

  update_inputs: function(state,action){
    console.log('reducer: update_inputs');
    var id = action.arguments[0];
    var value = action.arguments[1];
    var [section,name] = id.split('.');
    state.inputs[section] = state.inputs[section] || {};
    // state.inputs = {};
    value = f.str_to_num(value);
    if(value==='') value = undefined;
    state.inputs[section][name] = value;

    // console.log(state.inputs);
    if( name === 'manufacturer_name' ){
      state.inputs[section]['device_name'] = undefined;
    }

    sessionStorage.setItem('inputs', JSON.stringify(state.inputs));
    state = mk_system_data(state);
    return state;
  },

  clear_inputs: function(state,action){
    state.inputs = {};
    state = mk_system_data(state);
    return state;
  }


};
