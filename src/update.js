import {div, span, p, a, ul, li, br, h1, h2, h3, input, select, option} from 'specdom_helper';
import pack_data from './pack_data';
import get_drawing  from './get_drawing';
import db from './db';
import $ from 'specdom';

// var specdom_main = Specdom('#content');

export default function update(state,actions){
  global.state = state;
  var system = state.system;
  var inputs = state.inputs;

  // console.log(inputs);
  var system_code = pack_data(inputs);
  window.location.hash = '/'+system_code;

  var svgs;

  var inverter_manufacturers = [''].concat(Object.keys(db.inverter_tree));
  // var inverter_manufacturer_input = document.getElementById('inverter.manufacturer') || false;
  var inverter_models = system.inverter && system.inverter.manufacturer_name ? [''].concat(Object.keys(db.inverter_tree[system.inverter.manufacturer_name])) : [];
  // var optimizer_manufacturers = [''].concat(Object.keys(db.optimizer_tree));
  // var optimizer_manufacturer_input = document.getElementById('optimizer.manufacturer') || false;
  // system.optimizer.manufacturer_name = 'SolarEdge';
  var optimizer_models = system.optimizer && system.optimizer.manufacturer_name ? [''].concat(Object.keys(db.optimizer_tree[system.optimizer.manufacturer_name])) : [];
  var module_manufacturers = [''].concat(Object.keys(db.module_tree));
  // var module_manufacturer_value = document.getElementById('module.manufacturer') || false;
  var module_models = system.module && system.module.manufacturer_name ? [''].concat(Object.keys(db.module_tree[system.module.manufacturer_name])) : [];

  var option_lists = {
    'module.manufacturer_name': module_manufacturers,
    'module.device_name': module_models,
    'module.num_of_strings': ['1','2','3','4'],
    'inverter.manufacturer_name': inverter_manufacturers,
    'inverter.device_name': inverter_models,
    'optimizer.device_name': optimizer_models,
    'interconnection.connection_point': ['','Load side','Supply side'],
    'interconnection.ac_combiner': ['No','Yes'],
  };

  // console.log('state',state);
  // console.log(system.inverter);
  if(system.inverter.system_type === 'optimizer'){
    $('#optimizer.device_name').attr('disabled',null).parent().attr('disabled',null);
  } else {
    $('#optimizer.device_name').attr('disabled',true).parent().attr('disabled',true);
  }
  if(system.inverter.system_type === 'micro'){
    $('#interconnection.ac_combiner').attr('disabled',null).parent().attr('disabled',null);
  } else {
    $('#interconnection.ac_combiner').attr('disabled',true).parent().attr('disabled',true);
  }
  var inputs_elements = document.getElementsByClassName('input');
  for (var i = 0; i < inputs_elements.length; i++) {
    var input = inputs_elements[i];
    var section,name;
    [section,name] =  input.id.split('.');
    if( input.tagName === 'INPUT' ){
      input.value = inputs[section][name] !== undefined ? inputs[section][name] : '';
    }
    if( input.tagName === 'SELECT' ){
      input.options.length = 0;
      for( var index in option_lists[input.id]) {
        input.options[input.options.length] = new Option(option_lists[input.id][index], option_lists[input.id][index]);
      }
      input.value = inputs[section][name] !== undefined ? inputs[section][name] : '';
    }

  }

  system.interconnection.connection_point =
    {
      'Load side': 'load',
      'Supply side': 'supply',
    }[system.interconnection.connection_point];

  get_drawing(system, function(results){
    var system_settings = results.system_settings;
    var status = system_settings.state.notes.errors.length ? 'error' : 'pass';
    if( status === 'pass'){
      console.log('System passes');

      svgs = system_settings.drawing.svgs.map(function(svg){
        return svg.outerHTML;
      });

    } else {
      console.log('errors:' ,system_settings.state.notes);
    }

    var drawing_section = $('#drawing_section');
    if( svgs || system_settings.state.notes ){
      drawing_section.attr('hidden',null);
    } else {
      drawing_section.attr('hidden',true);
    }

    if(system_settings.state.notes){
      var notes_specs = [];

      if(system_settings.state.notes.errors.length){
        notes_specs.push(span('Errors:'));
        notes_specs.push(ul(system_settings.state.notes.errors.map(text => li(text))));
      }
      if(system_settings.state.notes.warnings.length){
        notes_specs.push(span('Warnings:'));
        notes_specs.push(ul(system_settings.state.notes.warnings.map(text => li(text))));
      }
      if(system_settings.state.notes.info.length){
        notes_specs.push(span('Info:'));
        notes_specs.push(ul(system_settings.state.notes.info.map(text => li(text))));
      }

      var specdom_notes = $('#notes');
      specdom_notes.clear();
      specdom_notes.append( notes_specs );
    }
    var specdom_svgs = $('#svgs');
    if(svgs){
      var svg_specs = svgs.map(svg => span({innerHTML:svg}));
      specdom_svgs.clear();
      specdom_svgs.append( svg_specs );
    } else {
      specdom_svgs.clear();
    }
  });


}
