/**
* this is the app
* @file_overview this the starting point for the application.
* @author keith showalter {@link https://github.com/kshowalter}
* @version 0.1.0
*/
console.log('/\\');

import 'normalize.css';

import hash_router from 'hash_router';

import mkwebsite from 'mkwebsite';
import update from './update';
import reducers from './reducers';
import mk_page from './mk_page';
import db from './db';
import mk_system_data from './mk_system_data';
import pack_data from './pack_data';


var global = window || global;
var logger = global.logger = {info:console.log,warn:console.warn,error:console.error};


var inputs;
if(window.location.hash){
  var system_code = window.location.hash;
  if( system_code.slice(0,2) === '#/' ){
    system_code = system_code.slice(2);
  }
  if(system_code){
    inputs = pack_data(system_code);
  }
}

if( ! inputs && sessionStorage.getItem('inputs') ){
  inputs = JSON.parse(sessionStorage.getItem('inputs'));
}


var init_state = {
  db,
  inputs,
};
init_state = mk_system_data(init_state);

var actions = mkwebsite(init_state, reducers, update);

var selected_page_id = 'default';
mk_page(actions, selected_page_id);
