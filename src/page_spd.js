import $ from 'specdom';
import f from 'functions';
import {div, span, p, a, ul, li, br, h1, h2, h3, input, select, option, table, tr, th, td, col } from 'specdom_helper';
import db from './db';

var local_actions;

function mkinput(class_name, label, id, flags){
  id = id || 'misc.input_' + f.name_to_id(label);
  // default_value = default_value || '';

  var specs = {
    tag: 'span',
    props: {
      class: 'input_group ' + class_name,
      onchange: function(e){
        local_actions.update_inputs(e.target.id,e.target.value);
      }
    },
    children: [
      span(label),
      input({
        class: 'input',
        id: id,
        // value: default_value,
      }),
    ]
  };
  if(flags){
    flags.forEach( flag => {
      specs.props[flag] = true;
      specs.children[1].props[flag] = true;
    } );
  }
  return specs;
}

function mklist(class_name, list, label, id, flags){
  id = id || 'misc.input_' + f.name_to_id(label);
  class_name = class_name || 'input';
  var specs = {
    tag: 'span',
    props: {
      class: 'input_group ' + class_name,
      onchange: function(e){
        local_actions.update_inputs(e.target.id,e.target.value);
      }
    },
    children: [
      span(label),
      select({class:'input',id:id},list.map(name => {
        // if( name == default_value ){
        // return option({selected:true},name);
        // } else {
        return option(name);
        // }
      })),
    ]
  };
  // console.log(flags);
  if(flags){
    flags.forEach( flag => {
      specs.props[flag] = true;
      specs.children[1].props[flag] = true;
    } );
  }
  return specs;
}

export default function(actions){
  local_actions = actions;

  var page_spec = div([
    div({class:'page_container'},[
      span({class:'page_title'},'System configuration'),
      div({class:'page'},[

        div({class:'section_title'},'- PV array -'),
        div(['Choose the PV module the system uses and indicate the combined number of strings on all inverters, the number of modules in the smallest string(s), and the number of modules in the largest string(s), and the total number of modules for that model among all inverters. For systems using microinverters or ACPV modules, please provide information for branch circuits rather than series strings. If the module is not listed, please submit an ',a('issue','https://gitlab.com/mechkit/spd/issues'),'.']),
        div({'class':'input_line'},[
          mklist('input_list_wide',[],'Manufacturer','module.manufacturer_name'),
          mklist('input_list_mid',[],'Model','module.device_name'),
          // span({class:'input_group'},[
          //   span({class:''},'Module Info'),
          //   span({class:'input_button'},'Module Info'),
          // ]),
          mklist('input_list_narrow',[],'# of Strings','module.num_of_strings'),
          mkinput('input_int','Smallest String','module.smallest_string'),
          mkinput('input_int','Largest String','module.largest_string'),
          mkinput('input_int','# of Modules','module.num_of_modules'),
          mkinput('input_int','PV Module\'s distance above roof (in)','module.array_offset_from_roof'),
        ]),

        div({class:'section_title'},'- Power Conditioning -'),
        div(['Select the inverter model the system uses, the quantity of that inverter being used, and the operating voltage. If the inverter is not listed, please submit an ',a('issue','https://gitlab.com/mechkit/spd/issues'),'.']),
        div({'class':'input_line'},[
          mklist('input_list_narrow',[],'Manufacturer','inverter.manufacturer_name'),
          mklist('input_list_wide',[],'Model','inverter.device_name'),
          mklist('input_list_narrow',[],'Manufacturer','optimizer.manufacturer_name', ['hidden']),
          mklist('input_list_narrow',[],'Optimizer Model','optimizer.device_name', ['disabled']),
          // span({class:'input_group'},[
          //   span({class:''},'Inverter Info'),
          //   span({class:'input_button'},'Inverter Info'),
          // ]),
          // mkinput('','AC Output Voltage (V)','inverter.grid_voltage'),
        ]),

        div({class:'section_title'},'- Interconnection -'),
        div({'class':'input_line'},[
          mklist('input_list_narrow',[],'AC combiner or sub-panel?','interconnection.ac_combiner', ['disabled']),
          mklist('input_list_narrow',[],'Connection point','interconnection.connection_point'),
          mkinput('input_int','Busbar Rating (A)','interconnection.bussbar_rating'),
          mkinput('input_int','Supply OCPD Rating (A)','interconnection.supply_ocpd_rating'),
          mkinput('input_int','Total Of Load Breakers (A)','interconnection.load_breaker_total'),
        ]),
        div({class:'section_title'},' '),
        div({'class':'input_line'},[
          div({class:'question_black'},[
            span('clear inputs?', {class:'',onclick:function(){
              $('.confirm_clear').forEach( target => {
                target.attr('hidden',null);
              });
            }})
          ]),
          span('really?',{hidden:'true',class:'confirm_clear question_black'}),
          div({class:'confirm_clear',hidden:'true'},[
            span('no',{class:'question_green',onclick:function(){
              $('.confirm_clear').forEach( target => {
                target.attr('hidden',true);
              });
            }}),
          ]),
          div({class:'confirm_clear',hidden:'true'},[
            span('yes',{class:'question_red',onclick:function(){
              $('.confirm_clear').forEach( target => {
                target.attr('hidden',true);
              });
              actions.clear_inputs();
            }}),
          ]),
        ]),
      ]),

      div({id:'drawing_section',hidden:'true'},[
        span({class:'page_title'},'Drawing'),
        div({class:'page'},[
          div({id:'notes',class:'notes'}),
          div({id:'svgs',class:'svgs'}),
        ]),
      ]),

    ]),

  ]);


  return page_spec;
}
