var value_titles = [
  'inverter.manufacturer_name',
  'inverter.device_name',
  '',
  '',
  'optimizer.manufacturer_name',
  'optimizer.device_name',
  '',
  '',
  'module.manufacturer_name',
  'module.device_name',
  'module.num_of_strings',
  'module.smallest_string',
  'module.largest_string',
  'module.num_of_modules',
  'module.array_offset_from_roof',
  '',
  '',
  'interconnection.ac_combiner',
  'interconnection.connection_point',
  'interconnection.bussbar_rating',
  'interconnection.supply_ocpd_rating',
  'interconnection.load_breaker_total',
  '',
  '',
];

export default function(input){
  var output;
  if( input.constructor === String ){ // Decode
    output = {};

    input = atob(input);
    input = input.split(',');
    input = input.forEach((value,i)=>{
      var value_title = value_titles[i];
      [section_name,value_name] = value_title.split('.');
      if(section_name){
        value = value.replace(/~/g,',');
        if(!isNaN(value)){
          value = Number(value);
        }
        if(value==='') value = undefined;
        output[section_name] = output[section_name] || {};
        output[section_name][value_name] = value;
      }
    });
  } else if( input.constructor === Object ){ // encode
    output = [];
    for( var section_name in input ){
      for( var value_name in input[section_name] ){
        var value_title = section_name+'.'+value_name;
        var ind = value_titles.indexOf(value_title);
        output[ind] = String(input[section_name][value_name]);
      }
    }
    output = [...Array(output.length)].map((_, i)=>{
      if(output[i]) return output[i].replace(/,/g,'~');
      else return '';
    });
    output = JSON.stringify(output);
    output = output.replace(/"/g,'');
    output = output.slice(1,-1);
    output = btoa(output);

  } else {
    console.warn('I do not know how to handle this', input);
  }


  return output;
}
